core 0.8
====

*(not officially released, yet)*

Basic universal sensors
---

* EnemyTeamIDs
* EnemyUnits
* HaveUnits
* FilterUnitsByCategory

* MissionInfo

* PointUnitDefValue
* Position

* TeamEnergy
* TeamMetal
* TeamUnitCountByCategory
* TeamUnitCountByName

* UnitDefValue
* UnitDefValueStatic
* UnitsCount

Basic universal actions
---

* echo
* setRepeat
