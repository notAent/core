function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Print any variable including tables into log.",
		parameterDefs = {
			{ 
				name = "SpringEcho",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "enabled",
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "true",
			}
		}
	}
end

local TAB = "    "

function PrintTable(tableToPrint, indentationString)
	local nextIndentationString = indentationString .. TAB

	for key, value in pairs(tableToPrint) do
		if (type(value) == "table") then
			Spring.Echo(indentationString .. "{")

			PrintTable(value, nextIndentationString)
			
			Spring.Echo(indentationString .. "},")		
		else
			Spring.Echo(indentationString .. "[" .. key .. "] = " .. tostring(value) .. ",")
		end
	end
end

function Run(self, unitIds, p)
	local toPrint = p.SpringEcho
	local enabled = p.enabled
	
	if (enabled) then
		if (type(toPrint) == "table") then
			local startIndentation = ""
			Spring.Echo(startIndentation .. "TABLE = {")
			PrintTable(toPrint, startIndentation .. TAB)
			Spring.Echo(startIndentation .. "}")
		else
			Spring.Echo(toPrint)
		end
	end
	return SUCCESS
end