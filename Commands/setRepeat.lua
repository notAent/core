function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Set repeat flag of given units to wanted value",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "units",
			},
			{ 
				name = "enabled",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
		}
	}
end

-- constants
local CMD_REPEAT = CMD.REPEAT
local REPEAT_OFF_STATE = 0
local REPEAT_ON_STATE = 1

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
end

function Run(self, units, parameter)
	local listOfUnits = parameter.listOfUnits -- array
	local enabled = REPEAT_OFF_STATE
	if (parameter.enabled) then enabled = REPEAT_ON_STATE end
	
	for i=1, #listOfUnits do
		Spring.GiveOrderToUnit(listOfUnits[1], CMD.REPEAT, {enabled}, {})
	end
	
	return SUCCESS
end

function Reset(self)
	ClearState(self)
end
