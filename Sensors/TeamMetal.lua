local sensorInfo = {
	name = "TeamMetal",
	desc = "Return actual value of team metal resources information",
	author = "PepeAmpere",
	date = "2017-04-28",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetTeamResources = Spring.GetTeamResources
local myTeamID = Spring.GetMyTeamID()

-- @description return metal resource info
return function()
	local currentLevel, storage, pull, income, expense, share, sent, received = SpringGetTeamResources(myTeamID, "metal")
	return { 
		currentLevel = currentLevel,
		storage = storage,
		pull = pull,
		income = income,
		expense = expense,
		share = share,
		sent = sent,
		received = received,
	}
end