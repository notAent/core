local sensorInfo = {
	name = "EnemyTeamIDs",
	desc = "Returns list of enemy team IDs",
	author = "PepeAmpere",
	date = "2018-05-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description returns list of enemy teamIDs
return function()
	local teams = Spring.GetTeamList()
	local myTeamID = Spring.GetMyTeamID()
	local gaiaID = Spring.GetGaiaTeamID()
	local enemyTeams = {}
	
	for i=1, #teams do
		local teamID = teams[i]
		if ((teamID ~= myTeamID) and (teamID ~= gaiaID) and not Spring.AreTeamsAllied(teamID, myTeamID)) then
			enemyTeams[#enemyTeams + 1] = teamID
		end
	end
	
	return enemyTeams
end