local sensorInfo = {
	name = "UnitsCount",
	desc = "Return number of units present in given context.",
	author = "PepeAmpere",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	return #units
end