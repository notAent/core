local sensorInfo = {
	name = "HaveUnits",
	desc = "Return true if there are some units to assinged/to play with in given context.",
	author = "PepeAmpere",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	return Sensors.UnitsCount() > 0
end