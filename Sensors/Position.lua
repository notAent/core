local sensorInfo = {
	name = "Position",
	desc = "Return position of the point unit.",
	author = "PepeAmpere",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function()
	local x,y,z = SpringGetUnitPosition(units[1])
	return Vec3(x,y,z)
end