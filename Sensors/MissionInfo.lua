local sensorInfo = {
	name = "MissionInfo",
	desc = "Mission information data",
	author = "PepeAmpere",
	date = "2018-05-01",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message")

local SpringGetRulesParam = Spring.GetGameRulesParam

-- @description return mission info
return function()
	local data = SpringGetRulesParam("MissionInfo")
	return message.Decode(data) or {}
end