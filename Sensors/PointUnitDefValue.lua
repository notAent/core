local sensorInfo = {
	name = "PointUnitDefValue",
	desc = "Return unit definition parameter value of the point unit based on its name.",
	author = "PepeAmpere",
	date = "2017-05-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitDefID = Spring.GetUnitDefID

-- @description return definition value of first unit
-- @argument paramName [string] name of the parameter
return function(paramName)
	return Sensors.UnitDefValue(units[1], paramName)
end