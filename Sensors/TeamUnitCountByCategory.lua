local sensorInfo = {
	name = "TeamUnitCountByCategory",
	desc = "Return number of units of given category in whole team",
	author = "PepeAmpere",
	date = "2017-05-15",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetTeamUnitsCounts= Spring.GetTeamUnitsCounts
local myTeamID = Spring.GetMyTeamID()

-- @description return energy resource info
-- @argument category [table] BETS category 
return function(category)
	local unitsCounts = SpringGetTeamUnitsCounts(myTeamID)
	local finalCount = 0
	if (unitsCounts ~= nil) then
		for unitDefID, count in pairs(unitsCounts) do
			if (category[unitDefID] ~= nil) then
				finalCount = finalCount + count
			end
		end
	end
	
	return finalCount
end